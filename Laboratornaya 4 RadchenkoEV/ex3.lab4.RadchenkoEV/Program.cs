﻿using System;

namespace ex3.lab4.RadchenkoEV
{
    class Program
    {
        static void Main(string[] args)
        {
            double k = Convert.ToDouble(Console.ReadLine());
            y(k);
        }
        static double y(double k)
        {
            double result = (3 * k - 13) / 31.0;
            Console.WriteLine(result);
            return result;
        }
        static double SUM(double k) => (k == 1) ? y(1) : y(k) + SUM(k-1);
    }
}
