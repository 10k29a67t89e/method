﻿using System;

namespace ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;
            double z, c;
            x = Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($" х = {x}  и  y = {y}");
            z = -3 + x;
            c = y + 13.0 / 3.0;
            Max(c, z);
            Console.WriteLine($"{ Max(c, z)} ");
        }
        static int Max(int c, int z)
        {
            return Math.Max(c, z);
        }
        static double Max(double c, double z)
        {
            return Math.Max(c, z);
        }
        static char Max(char c, char z)
        {
          c =  Convert.ToChar(c);
          z = Convert.ToChar(z);
          return (char) Math.Max(c, z);
        }

    }
}

